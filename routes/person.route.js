require('dotenv').load();

const express = require('express');
const moment = require('moment-timezone');
const Person = require('../models/person.model');
const EmailService = require('../services/email.service');

module.exports = (app) => {
    const router = express.Router();
    app.use('/person', router);

    const prepareUserForEmail = (user) => {
        const datetime = moment(user.datetime).tz('America/Sao_Paulo');
        const apiUrl = process.env.API_URL;
        const updateUrl = `${process.env.APP_URL}/update`;

        return {
            email: user.email,
            name: user.name,
            topic: user.topic,
            confirm: `${apiUrl}/person/${user._id}/confirm`,
            cancel: `${apiUrl}/person/${user._id}/cancel`,
            update: `${updateUrl}/${user._id}`,
            monthday: `${datetime.format('DD')}`,
            time: `${datetime.format('HH')}h${datetime.format('mm')}`
        };
    };

    router.get('/', (req, res) => {
        res.status(200).json({ message: 'person API OK' });
    });

    router.get('/:id', (req, res) => {
        Person.get(req.params.id)
            .then((result) => {
                if(result === null) throw 'ID NOT FOUND';

                res.status(200).send({
                    success: true,
                    message: 'Pessoa encontrada',
                    data: result });
            }).catch((err) => {
                res.status(500).send({
                    success: false,
                    message: 'Erro: pessoa não foi encontrada',
                    data: `${err}` });
            });
    });

    router.post('/', function (req, res) {
        Person.create(req.body)
            .then((result) => {
                const user = prepareUserForEmail(result);

                EmailService.sendConfirmEmail(user).then(() => {
                    res.status(201).send({
                        success: true,
                        message: 'Pessoa criada',
                        data: result });
                });
            }).catch((err) => {
                res.status(500).send({
                    success: false,
                    message: 'Erro: pessoa não foi criada',
                    data: `${err}` });
            });
    });

    router.post('/:id', function (req, res) {
        Person.get(req.params.id).then((person) => {
            person.confirmed = false;
            person = Object.assign(person, req.body);

            Person.update(person).then((updatedPerson) => {
                const user = prepareUserForEmail(updatedPerson);

                EmailService.sendConfirmEmail(user).then(() => {
                    res.status(201).send({
                        success: true,
                        message: 'Pessoa atualizada',
                        data: updatedPerson });
                });
            });
        }).catch((err) => {
            res.status(500).send({
                success: false,
                message: 'Erro: pessoa não foi atualizada',
                data: `${err}` });
        });
    });

    router.get('/:id/confirm', (req, res) => {
        Person.get(req.params.id).then((person) => {
            person.confirmed = true;
            Person.update(person).then(() => {
                res.redirect(`${process.env.APP_URL}/confirmed`);
            });
        }).catch((err) => {
            res.status(500).send({
                success: false,
                message: 'Erro: pessoa não foi confirmada',
                data: `${err}` });
        });
    });

    router.get('/:id/cancel', (req, res) => {
        Person.delete(req.params.id).then(() => {
            res.redirect(`${process.env.APP_URL}/canceled`);
        }).catch((err) => {
            res.status(500).send({
                success: false,
                message: 'Erro: pessoa não foi removida',
                data: `${err}` });
        });
    });
};

module.exports.Person = Person;
module.exports.EmailService = EmailService;
