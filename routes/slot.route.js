const express = require('express');
const moment = require('moment-timezone');

const Slot = require('../models/slot.model');

module.exports = (app) => {
    const router = express.Router();
    app.use('/slots', router);

    router.get('/', (req, res) => {
        Slot.getAll().then((result) => {
            res.json(result);
        }).catch((err) => {
            res.status(500).send({
                success: false,
                message: 'Erro na busca de horários',
                data: `${err}` });
        });
    });

    router.get('/available', (req, res) => {
        Slot.getAll().then((result) => {
            const available = {};
            const now = moment(new Date()).tz('America/Sao_Paulo');

            result.forEach((slot) => {
                slot = JSON.parse(JSON.stringify(slot));

                const datetime = moment(slot.datetime).tz('America/Sao_Paulo');
                const date = datetime.format('YYYY-MM-DD');
                if (!(date in available)) {
                    available[date] = {
                        slots: [],
                        available: false
                    };
                }

                slot.available = (slot.persons.length < Slot.MAX_PER_SLOT) &&
                    (now.isBefore(datetime));
                delete slot.persons;

                available[date].slots.push(slot);
                available[date].available = available[date].available || slot.available;
            });

            res.json(available);
        }).catch((err) => {
            res.status(500).send({
                success: false,
                message: 'Erro na busca de horários disponíveis',
                data: `${err}` });
        });
    });

    router.get('/taken', (req, res) => {
        Slot.getTaken().then((result) => {
            res.json(result);
        }).catch((err) => {
            res.status(500).send({
                success: false,
                message: 'Erro na busca de horários disponíveis',
                data: `${err}` });
        });
    });
};

module.exports.Slot = Slot;
