const fs = require('fs');
const AWS = require('aws-sdk');
const Mustache = require('mustache');
var htmlToText = require('html-to-text');

AWS.config.update({ region: process.env.AWS_REGION });

const EmailService =  {
    confirmHtml: function(user) {
        return Mustache.render(
            fs.readFileSync('./services/data/confirm.html', 'utf8'),
            {user: user});
    },

    confirmText: function(user) {
        return htmlToText.fromString(this.confirmHtml(user), {
            wordwrap: 130,
            preserveNewlines: true,
            ignoreImage: true
        }).trim().replace(/\n\n\n/g, '\n');
    },

    sendConfirmEmail: function(user) {
        const params = {
            Destination: {
                ToAddresses: [ user.email ]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: 'UTF-8',
                        Data: this.confirmHtml(user)
                    },
                    Text: {
                        Charset: 'UTF-8',
                        Data: this.confirmText(user)
                    }
                },
                Subject: {
                    Charset: 'UTF-8',
                    Data: 'Agendamento de Consultoria Relâmpago'
                }
            },
            Source: '"ThoughtWorks - Arena Coragem" <arenacoragem@thoughtworks.com>'
        };
        return new AWS.SES({apiVersion: '2010-12-01'})
            .sendEmail(params).promise();
    }
};

module.exports = EmailService;
