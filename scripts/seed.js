require('dotenv').load();

const mongoose = require('mongoose');
const winston = require('winston');

const Slot = require('../models/slot.model');
const Person = require('../models/person.model');

function log(msg) {
    if (require.main === module) {
        winston.info(msg);
    }
}

function seed(cb, db) {
    let slots = [];
    db = db || process.env.MONGODB_URL;

    function createSlot(datetime) {
        return new Promise(function(resolve, reject) {
            const slot = new Slot({
                datetime: datetime,
                persons: []
            });

            const searchQuery = {
                $and: [
                    { 'confirmed': { $eq: true } },
                    { 'datetime': { $eq : slot.datetime } }
                ]
            };

            Person.find(searchQuery).then((persons) => {
                slot.persons = persons;
                slot.save().then((saved) => {
                    log('saved ' + saved.datetime);
                    resolve(saved);
                });
            }).catch((err) => reject(err));
        });
    }

    mongoose.connect(db)
        .then(() => Slot.remove({ }))
        .then(() => {
            slots.push(createSlot(new Date('2018-11-05T11:00:00-02:00')));
            slots.push(createSlot(new Date('2018-11-05T16:40:00-02:00')));
            slots.push(createSlot(new Date('2018-11-05T17:30:00-02:00')));
            slots.push(createSlot(new Date('2018-11-06T11:00:00-02:00')));
            slots.push(createSlot(new Date('2018-11-06T16:40:00-02:00')));
            slots.push(createSlot(new Date('2018-11-06T17:30:00-02:00')));
            slots.push(createSlot(new Date('2018-11-07T11:00:00-02:00')));
            slots.push(createSlot(new Date('2018-11-07T16:40:00-02:00')));
            Promise.all(slots).then(() => {
                log('done');
                if (require.main === module) {
                    mongoose.disconnect();
                }
                if(cb) cb();
            });
        }).catch((err) => {
            winston.error('while saving slots: ' + err);
        });
}

module.exports = seed;

if (require.main === module) {
    seed();
}
