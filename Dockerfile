FROM node:8-stretch

ADD package.json /tmp/package.json
RUN cd /tmp && npm install
RUN mkdir -p /opt/relampago-api && cp -a /tmp/node_modules /opt/relampago-api/

ADD models/ /opt/relampago-api/models/
ADD routes/ /opt/relampago-api/routes/
ADD scripts/ /opt/relampago-api/scripts/
ADD services/ /opt/relampago-api/services/
ADD test/ /opt/relampago-api/test/

ADD index.js /opt/relampago-api/
ADD package.json /opt/relampago-api/

WORKDIR /opt/relampago-api

CMD ["npm", "run", "start"]
