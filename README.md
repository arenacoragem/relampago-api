[![Build Status](https://travis-ci.org/arenacoragem/relampago-api.svg?branch=master)](https://travis-ci.org/arenacoragem/relampago-api)

# relampago-api
Backend para o app de agendamento de consultorias relâmpago.

Você precisa de:
- nodejs (versão 8)
- npm (versão >= 5)
- mongodb na porta 27017
- arquivo ```.env```

### .env
Guardamos alguns segredos, chaves e urls no arquivo ```.env```. O arquivo ```.env.example``` pode ser copiado para ```.env``` e usado durante desenvolvimento local. As únicas variáveis que precisam ser preenchidas são as da AWS.

### mongo
O comando para rodar o mongodb, especificando o diretório onde armazenar o banco:
```
mongod --dbpath ./db
```

### node
Para instalar dependências, testar, inicializar o banco e rodar o servidor:
```
npm install
npm run test
npm run seed
npm run start
```

O servidor deve subir em http://localhost:5000/

### Se preferir rodar usando Docker

**Para rodar testes:**
```
docker-compose build
docker-compose run api npm run test
```

**Para rodar o servidor:**
```
docker-compose build
docker-compose run api npm run seed
docker-compose up
```

As vezes o primeiro comando de ```docker-compose run``` falha porque o servidor sobe antes do banco de dados. Quando isso acontece é só esperar um pouquinho e rodar o mesmo comando novament. Estamos investigando soluções mais robustas.
