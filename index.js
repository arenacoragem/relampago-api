require('dotenv').load();

const mongoose = require('mongoose');
const winston = require('winston');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');
const moment = require('moment-timezone');

const PORT = process.env.PORT || 5000;
const app = express();

mongoose.connect(process.env.MONGODB_URL)
    .then(() => {
        winston.info('Connected to database...');
    }).catch(() => {
        winston.error('Unable to connect to database...');
        server.close();
        process.exit(1);
    });

// ROUTES
const person = require('./routes/person.route');
const slot = require('./routes/slot.route');

const corsOptions = {
    origin: [
        process.env.CORS_TEST_ORIGIN,
        process.env.CORS_PROD_ORIGIN
    ],
    optionsSuccessStatus: 200
};

app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: false }));
app.use(bodyParser.json({ limit: '5mb' }));

app.get('/', (req, res) => {
    res.status(200).json({ message: 'API OK' });
});

person(app);
slot(app);

const server = app.listen(PORT, () => {
    winston.log('info', `Listening @ ${PORT}`);
});

setInterval(() => {
    const startTime = moment('06:00:00', 'hh:mm:ss').tz('America/Sao_Paulo');
    const endTime = moment('23:59:00', 'hh:mm:ss').tz('America/Sao_Paulo');
    const nowInSaoPaulo = moment(new Date()).tz('America/Sao_Paulo');
    if(nowInSaoPaulo.isBetween(startTime, endTime)) {
        http.get(process.env.API_URL);
    }
}, 30 * 60 * 1000);

module.exports = app;
