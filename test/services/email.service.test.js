require('dotenv').load();
const chai = require('chai');
const should = chai.should();
const EmailService = require('../../services/email.service');

let fixture = {
    email: 'thersan@thoughtworks.com',
    name: 'Foo Baz',
    topic: 'Liderança Adaptativa',
    confirm: 'https://foo.com/id/confirm',
    update: 'https://foo.com/id/update',
    cancel: 'https://foo.com/id/cancel',
    monthday: '06',
    time: '13h40'
};

describe('Email Service', () => {
    describe('Mustaching', () => {
        let html;
        let text;

        before(() => {
            html = EmailService.confirmHtml(fixture);
            text = EmailService.confirmText(fixture);
        });

        it('plaintext should have rendered all fields (except email)', (done) => {
            Object.keys(fixture).filter(k => (k !== 'email')).forEach((k) => {
                text.should.include.string(fixture[k]);
            });
            done();
        });

        it('html should have rendered all fields (except email)', (done) => {
            Object.keys(fixture).filter(k => (k !== 'email')).forEach((k) => {
                html.should.include.string(fixture[k]);
            });
            done();
        });

        it('plaintext should not have html tags', (done) => {
            should.equal(EmailService.confirmText(fixture).match(/<[^>]*>/g), null);
            done();
        });
    });

    describe('sendConfirmEmail()', () => {
        it('returns a Promise', (done) => {
            EmailService.sendConfirmEmail(fixture).should.be.a('Promise');
            done();
        });
    });
});
