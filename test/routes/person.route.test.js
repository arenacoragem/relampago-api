const stub = require('sinon').stub;
const chai = require('chai');
chai.use(require('chai-http'));

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json({ limit: '5mb' }));

const person = require('../../routes/person.route');

describe('Person Routes', () => {
    person(app);

    const newPersonData = {
        name: 'new person',
        email: 'new@company.com',
        company: 'my company',
        role: 'a new type of boss',
        topic: 'novidades',
        confirmed: false,
        datetime: new Date('2018-11-05T11:00:00-02:00')
    };

    const updatedPersonData = {
        topic: 'a new tema',
        datetime: '2018-11-05T18:40:00.000Z',
        name: 'another new name',
        email: 'more@new.com'
    };

    const confirmedPersonData = Object.assign({}, newPersonData, {confirmed: true});

    it('GET /person should return 200', (done) => {
        chai.request(app).get('/person').end((err, res) => {
            res.should.have.status(200);
            done();
        });
    });

    it('GET /person/:id should fail when :id is INVALID', (done) => {
        const get = stub(person.Person, 'get').resolves(null);

        chai.request(app).get('/person/INVALID').end((err, res) => {
            res.should.have.status(500);
            res.body.should.have.all.keys('success', 'message', 'data');
            res.body.success.should.be.false;
            get.calledOnce.should.be.true;

            get.restore();
            done();
        });
    });

    it('GET /person/:id should return person when :id is VALID', (done) => {
        const newPerson = new person.Person(newPersonData);
        const get = stub(person.Person, 'get').resolves(newPerson);

        chai.request(app).get('/person/'+newPerson._id).end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.all.keys('success', 'message', 'data');
            res.body.success.should.be.true;
            get.calledOnce.should.be.true;

            res.body.data.should.have.all.keys([
                'name', 'email', 'company', 'role',
                'topic', 'confirmed', 'datetime', '_id'
            ]);

            get.restore();
            done();
        });
    });

    describe('POST /person', () => {
        let newPerson;
        let create;
        let send;
        let post;

        beforeEach(() => {
            newPerson = new person.Person(newPersonData);
            create = stub(person.Person, 'create').resolves(newPerson);
            send = stub(person.EmailService, 'sendConfirmEmail').resolves({});
            post = chai.request(app).post('/person').send(newPersonData);
        });

        afterEach(() => {
            create.restore();
            send.restore();
        });

        it('should send email', (done) => {
            post.end((err, res) => {
                res.should.have.status(201);
                res.body.success.should.be.true;
                send.calledOnce.should.be.true;

                send.getCall(0).args[0].should.have.all.keys(
                    'email',
                    'name',
                    'topic',
                    'confirm',
                    'cancel',
                    'update',
                    'monthday',
                    'time'
                );

                done();
            });
        });

        it('should return added person', (done) => {
            post.end((err, res) => {
                res.should.have.status(201);
                res.body.should.have.all.keys('success', 'message', 'data');
                res.body.success.should.be.true;
                create.calledOnce.should.be.true;

                res.body.data.should.have.all.keys([
                    'name', 'email', 'company', 'role',
                    'topic', 'confirmed', 'datetime', '_id'
                ]);
                res.body.data._id.should.equal(String(newPerson._id));

                done();
            });
        });
    });

    describe('POST /person/id', () => {
        let get, update;
        let send;
        let post;

        beforeEach(() => {
            get = stub(person.Person, 'get').resolves(confirmedPersonData);
            update = stub(person.Person, 'update').resolvesArg(0);
            send = stub(person.EmailService, 'sendConfirmEmail').resolves({});
            post = chai.request(app).post('/person/someId').send(updatedPersonData);
        });

        afterEach(() => {
            get.restore();
            update.restore();
            send.restore();
        });

        it('should send email', (done) => {
            post.end((err, res) => {
                res.should.have.status(201);
                res.body.should.have.all.keys('success', 'message', 'data');
                res.body.success.should.be.true;
                send.calledOnce.should.be.true;

                send.getCall(0).args[0].should.have.all.keys(
                    'email',
                    'name',
                    'topic',
                    'confirm',
                    'cancel',
                    'update',
                    'monthday',
                    'time'
                );

                done();
            });
        });

        it('should return updated person with new info', (done) => {
            post.end((err, res) => {
                res.should.have.status(201);
                get.calledOnce.should.be.true;
                update.calledOnce.should.be.true;

                res.body.data.should.have.all.keys([
                    'name', 'email', 'company', 'role',
                    'topic', 'confirmed', 'datetime'
                ]);

                res.body.data.topic.should.equal(updatedPersonData.topic);
                res.body.data.name.should.equal(updatedPersonData.name);
                res.body.data.email.should.equal(updatedPersonData.email);

                res.body.data.company.should.equal(newPersonData.company);
                res.body.data.role.should.equal(newPersonData.role);

                done();
            });
        });

        it('should return updated person NOT confirmed', (done) => {
            post.end((err, res) => {
                res.should.have.status(201);
                res.body.data.confirmed.should.equal(false);

                done();
            });
        });
    });

    it('GET /person/:id/confirm should confirm person', (done) => {
        const get = stub(person.Person, 'get').resolves(newPersonData);
        const update = stub(person.Person, 'update').resolves({});

        chai.request(app)
            .get('/person/VALID/confirm')
            .redirects(0)
            .end((err, res) => {
                res.should.have.status(302);
                get.calledOnce.should.be.true;
                update.calledOnce.should.be.true;

                update.getCall(0).args[0].should.have.property('confirmed', true);

                get.restore();
                update.restore();
                done();
            });
    });

    it('GET /person/:id/cancel should delete person', (done) => {
        const del = stub(person.Person, 'delete').resolves({});

        chai.request(app)
            .get('/person/VALID/cancel')
            .redirects(0)
            .end((err, res) => {
                res.should.have.status(302);
                del.calledOnce.should.be.true;

                del.restore();
                done();
            });
    });
});
