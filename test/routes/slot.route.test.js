const stub = require('sinon').stub;
const chai = require('chai');
chai.use(require('chai-http'));

const express = require('express');
const app = express();

const slot = require('../../routes/slot.route');

describe('Slots Routes', () => {
    slot(app);

    const available = [
        {
            persons: [],
            datetime: new Date('2118-11-05T11:00:00-02:00')
        },
        {
            persons: ['000000000000000000000000'],
            datetime: new Date('2118-11-06T11:00:00-02:00')
        }
    ];

    const taken = [
        {
            persons: [
                '111111111111111111111111',
                '222222222222222222222222'
            ],
            datetime: new Date('2118-11-07T11:00:00-02:00')
        }
    ];

    const all = available.concat(taken);

    it('GET /slots/ should return 3 slots', (done) => {
        const getAll = stub(slot.Slot, 'getAll').resolves(all);

        chai.request(app).get('/slots').end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.lengthOf(3);
            getAll.calledOnce.should.be.true;

            res.body.forEach((s) => {
                s.should.have.all.keys('datetime', 'persons');
            });

            getAll.restore();
            done();
        });
    });

    it('GET /slots/taken should return 1 slot', (done) => {
        const getTaken = stub(slot.Slot, 'getTaken').resolves(taken);

        chai.request(app).get('/slots/taken').end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.lengthOf(1);
            getTaken.calledOnce.should.be.true;

            res.body.forEach((s) => {
                s.should.have.all.keys('datetime', 'persons');
            });

            getTaken.restore();
            done();
        });
    });

    describe('GET /slots/available', () => {
        let getAll, res;

        before((done) => {
            getAll = stub(slot.Slot, 'getAll').resolves(all);
            chai.request(app).get('/slots/available').end((err, response) => {
                res = response;
                done();
            });
        });

        after((done) => {
            getAll.restore();
            done();
        });

        it('should return object with 3 keys', (done) => {
            res.should.have.status(200);
            Object.keys(res.body).should.have.lengthOf(3);
            getAll.calledOnce.should.be.true;
            done();
        });

        it('all dates should have correct keys', (done) => {
            Object.keys(res.body).forEach((date) => {
                res.body[date].should.have.all.keys('slots', 'available');
            });
            done();
        });

        it('dates should have correct availability status', (done) => {
            let takenCount = 0;
            Object.keys(res.body).forEach((date) => {
                takenCount += (res.body[date].available) ? 0 : 1;
            });
            takenCount.should.equal(1);
            done();
        });

        it('all slots should have correct keys', (done) => {
            Object.keys(res.body).forEach((date) => {
                res.body[date].slots.forEach((slot) => {
                    slot.should.have.all.keys('datetime', 'available');
                });
            });
            done();
        });

        it('slots should have correct availability status', (done) => {
            let takenCount = 0;
            Object.keys(res.body).forEach((date) => {
                res.body[date].slots.forEach((slot) => {
                    takenCount += (slot.available) ? 0 : 1;
                });
            });
            takenCount.should.equal(taken.length);
            done();
        });
    });
});
