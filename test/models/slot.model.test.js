const chai = require('chai');
chai.should();

const mongoose = require('mongoose');
const winston = require('winston');

const Slot = require('../../models/slot.model');
const Person = require('../../models/person.model');
const testData = require('../fixtures/persons.json');
const seed = require('../../scripts/seed');

describe('Slots Model', () => {
    let TOTAL_SLOTS;
    before((done) => {
        Slot.MAX_PER_SLOT = 2;
        TOTAL_SLOTS = 8;
        const ps = [];
        const db = `${process.env.MONGODB_URL}-test`;
        mongoose.connect(db)
            .then(() => {
                testData.Person.forEach((person) => {
                    ps.push((new Person(person)).save());
                });
                Promise.all(ps).then(() => {
                    seed(done, db);
                });
            }).catch((err) => {
                winston.error('Not connected to db: ' + err);
                process.exit(1);
            });
    });

    after((done) => {
        Person.remove({ }).then(() => {
            Slot.remove({ }).then(() => {
                done();
            });
        }).catch((err) => {
            winston.error('Not connected to db: ' + err);
            process.exit(1);
        });
    });

    it('.getAll() should return all slots', () => {
        return Slot.getAll().then((result) => {
            result.should.have.lengthOf(TOTAL_SLOTS);
        });
    });

    it('.getAvailable() should return all but 1 slot', () => {
        return Slot.getAvailable().then((result) => {
            result.should.have.lengthOf(TOTAL_SLOTS - 1);
        });
    });

    describe('.getTaken()', () => {
        it('should return 2 slots', () => {
            return Slot.getTaken().then((result) => {
                result.should.have.lengthOf(2);
            });
        });

        it('should have populated persons fields', () => {
            return Slot.getTaken().then((result) => {
                result.forEach((s) => {
                    s.persons.forEach((p) => {
                        p._doc.should.include.all.keys(
                            'datetime',
                            'name',
                            'company',
                            'role',
                            'topic');
                    });
                });
            });
        });
    });

    let newPerson;
    describe('.update when adding person', () => {
        before((done) => {
            Person.create(testData.Person[2]).then((saved) => {
                newPerson = saved;
                done();
            });
        });

        it('.getAvailable() should return one less slot', () => {
            return Slot.getAvailable().then((result) => {
                result.should.have.lengthOf(TOTAL_SLOTS - 2);
            });
        });

        it('.getTaken() should return 2 slots', () => {
            return Slot.getTaken().then((result) => {
                result.should.have.lengthOf(2);
            });
        });
    });

    describe('.update when delete person', () => {
        before((done) => {
            newPerson.confirmed = false;
            Person.delete(newPerson._id).then(() => {
                done();
            });
        });

        it('.getAvailable() should return one more slot', () => {
            return Slot.getAvailable().then((result) => {
                result.should.have.lengthOf(TOTAL_SLOTS - 1);
            });
        });

        it('.getTaken() should return 2 slots', () => {
            return Slot.getTaken().then((result) => {
                result.should.have.lengthOf(2);
            });
        });
    });
});
