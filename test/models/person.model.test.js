const spy = require('sinon').spy;
const chai = require('chai');
const should = chai.should();

const mongoose = require('mongoose');
const winston = require('winston');

const Slot = require('../../models/slot.model');
const Person = require('../../models/person.model');
const testData = require('../fixtures/persons.json');
const seed = require('../../scripts/seed');

describe('Person Model', () => {
    const newPersonData = {
        name: 'new person',
        email: 'new@company.com',
        company: 'company',
        role: 'new boss',
        topic: 'novidades',
        confirmed: false,
        datetime: new Date('2018-11-05T11:00:00-02:00')
    };
    const anotherNewPersonData = Object.assign({}, newPersonData);
    const ids = [];

    before((done) => {
        const ps = [];
        const db = `${process.env.MONGODB_URL}-test`;
        mongoose.connect(db)
            .then(() => {
                testData.Person.forEach((person) => {
                    ps.push((new Person(person)).save().then((p) => {
                        ids.push(p._id);
                    }));
                });
                Promise.all(ps).then(() => {
                    seed(done, db);
                });
            }).catch((err) => {
                winston.error('Not connected to db: ' + err);
                process.exit(1);
            });
    });

    const findOneArgs = { datetime: anotherNewPersonData.datetime };
    let updateSlotSpy, updateAllSlotsSpy, findOneSlotSpy;

    beforeEach(() => {
        updateSlotSpy = spy(Person, 'updateSlot');
        updateAllSlotsSpy = spy(Person, 'updateAllSlots');
        findOneSlotSpy = spy(Slot, 'findOne');
    });

    afterEach(() => {
        updateSlotSpy.restore();
        updateAllSlotsSpy.restore();
        findOneSlotSpy.restore();
    });

    after((done) => {
        Person.remove({ }).then(() => {
            Slot.remove({ }).then(() => {
                done();
            });
        }).catch((err) => {
            winston.error('Not connected to db: ' + err);
            process.exit(1);
        });
    });

    describe('.get()', () => {
        it('with valid id should return person', () => {
            return Person.get(ids[0]).then((result) => {
                result._doc.should.have.all.keys([
                    'name', 'email', 'company', 'role',
                    'topic', 'confirmed', 'datetime', '_id', '__v'
                ]);
                String(result._id).should.equal(String(ids[0]));
            });
        });

        it('with invalid id should not return person', () => {
            return Person.get('dead00beef00dead00beef00').then((result) => {
                should.equal(result, null);
            });
        });
    });

    describe('.create()', () => {
        it('should create and return a person', () => {
            return Person.create(newPersonData).then((result) => {
                newPersonData._id = result._id;
                result.should.have.property('email', newPersonData.email);
            });
        });

        it('should update the slot associated with the person', () => {
            return Person.create(anotherNewPersonData).then((result) => {
                anotherNewPersonData._id = result._id;
                updateSlotSpy.calledOnce.should.be.true;
                findOneSlotSpy.withArgs(findOneArgs).calledOnce.should.be.true;
            });
        });
    });

    describe('.update() with valid id', () => {
        it('should update person', () => {
            newPersonData.confirmed = true;
            return Person.update(newPersonData).then((result) => {
                result.should.have.property('confirmed', true);
            });
        });

        it('should update the slot associated with the person', () => {
            anotherNewPersonData.confirmed = true;
            return Person.update(anotherNewPersonData).then(() => {
                updateSlotSpy.called.should.be.false;
                updateAllSlotsSpy.calledOnce.should.be.true;
            });
        });
    });

    describe('.remove() with valid id', () => {
        it('should remove person', () => {
            return Person.delete(ids[0]).then((result) => {
                result.should.have.all.keys([
                    'name', 'email', 'company', 'role',
                    'topic', 'confirmed', 'datetime', '_id', '__v'
                ]);
                String(result._id).should.equal(String(ids[0]));
            });
        });

        it('should update the slot associated with the person', () => {
            return Person.delete(ids[1]).then(() => {
                updateSlotSpy.calledOnce.should.be.true;
                findOneSlotSpy.calledOnce.should.be.true;
            });
        });
    });
});
