const mongoose = require('mongoose');
const { Schema } = mongoose;

const SlotSchema = new mongoose.Schema({
    datetime: {
        type: Date,
        required: true
    },
    persons: [{
        type: Schema.Types.ObjectId,
        ref: 'Person'
    }]
});

const Slot = mongoose.model('Slot', SlotSchema);

Slot.MAX_PER_SLOT = 2;

Slot.getAll = function() {
    return Slot.find()
        .select('-_id datetime persons')
        .sort({ datetime: 1 });
};

Slot.getAvailable = function() {
    const searchQuery = { $where: `this.persons.length < ${Slot.MAX_PER_SLOT}` };
    return Slot.find(searchQuery)
        .select('-_id datetime persons')
        .sort({ datetime: 1 });
};

Slot.getTaken = function() {
    const searchQuery = { $where: 'this.persons.length > 0' };
    return Slot.find(searchQuery)
        .select('-_id datetime persons')
        .populate('persons', '-_id -__v -email')
        .sort({ datetime: 1 });
};

module.exports = Slot;
