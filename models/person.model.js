const mongoose = require('mongoose');

const Slot = require('./slot.model');

const PersonSchema = new mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    company: {
        type: String
    },
    role: {
        type: String
    },
    topic: {
        type: String,
        required: true
    },
    confirmed: {
        type: Boolean,
        default: false
    },
    datetime: {
        type: Date,
        required: true
    }
});

const Person = mongoose.model('Person', PersonSchema);

Person.updateSlot = function(person) {
    return new Promise((resolve, reject) => {
        Slot.findOne({ 'datetime': person.datetime }).then((slot) => {
            const searchQuery = {
                $and: [
                    { 'confirmed': { $eq: true } },
                    { 'datetime': { $eq : slot.datetime } }
                ]
            };

            Person.find(searchQuery).then((persons) => {
                slot.persons = persons;
                slot.save().then(() => {
                    resolve(person);
                });
            });
        }).catch(reject);
    });
};

Person.updateAllSlots = function(person) {
    return new Promise((resolve, reject) => {
        Slot.find().then((allSlots) => {
            allSlots.forEach((slot) => {
                const searchQuery = {
                    $and: [
                        { 'confirmed': { $eq: true } },
                        { 'datetime': { $eq : slot.datetime } }
                    ]
                };

                Person.find(searchQuery).then((persons) => {
                    slot.persons = persons;
                    slot.save().then(() => {
                        resolve(person);
                    });
                });
            });
        }).catch(reject);
    });
};

Person.get = function(id) {
    return Person.findById(id);
};

Person.create = function(data) {
    return (new Person(data)).save().then((person) => {
        return Person.updateSlot(person._doc);
    });
};

Person.update = function(data) {
    return Person.findOneAndUpdate({_id: data._id}, data, {new: true})
        .then((person) => {
            return Person.updateAllSlots(person._doc);
        });
};

Person.delete = function(id) {
    return Person.findOneAndRemove({ _id: id }).then((person) => {
        return Person.updateSlot(person._doc);
    });
};

module.exports = Person;
